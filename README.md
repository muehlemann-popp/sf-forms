## Symfony form extensions bundle.

Extra form types for Symfony framework.


### Requirements
* PHP >= 7.0

### Installation
##### Require via composer

```bash
composer require mpom/sf-forms
```
NOTE: that composer must be configured to use private repositories.

##### Enable it in bundles
```php
// app/AppKernel.php
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // other bundles
            new SfFormsBundle\SfFormsBundle(),
        );
    )
)
```

### Included types

[ImageType](docs/image-type.md)

[TranslatableType](docs/translatable-type.md)