## TranslatableType

A translation widget.

#### Options

```php
<?php
  // Bundle/Forms/AcmeForm.php
  
  public function buildForm($builder) {
    $builder->add('image', \SfFormsBundle\Form\Type\TranslatableType::class, [
      // a property in entity which keeps translations
      'translations_property' => 'translations',
      
      // an underlying widget
      'entry_type' => TextareaType::class,
      // options for the underlying widget
      'entry_options' => [
        'attr' => [
          'class' => 'form-control',
          'rows' => 7
        ]
      ],
      // a translations class to use
      // see https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/translatable.md#personal-translations
      'translations_class' => EntityTranslation::class,
      
      // a default locale
      'locale' => $this->locale,
      
      // a list of available locales
      'locales' => $this->locales,
      
      // a list of required locales
      'required_locales' => $this->requiredLocales,
    ])  
  
  }
```

