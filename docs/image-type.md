## ImageType

Provides image type with cropping and live preview.

#### Options

```php
<?php
  // Bundle/Forms/AcmeForm.php
  
  public function buildForm($builder) {
    $builder->add('image', \SfFormsBundle\Form\Type\ImageType::class, [
      // a web path to the image. used as a preview image
      'data' => $photo, 
      
      // enable cropping features.
      // note, current implementation does not crop image! 
      // it grabs crop data and sends it to the backend, so actual cropping 
      // must be implemeted independently of that widget.
      'enable_crop' => true,
      'crop_aspect_ratio' => 3/4,
      // a hidden or text input in which cropper will store the crop data
      'crop_save_to' => 'cropData',
      
      // enable preview
      'enable_preview' => true,
      // max width and height of preview image in pixels.
      'preview_width' => '273px',
      'preview_height' => null,
      
      // supported image types
      'image_types' => ['image/jpeg', 'image/png'],
      // an error message to display, when image type is not supported
      // default: 'Image of type "%format%" is not allowed. Choose one of: %allowed%.'.
      'msg_invalid_format' => 'error.js_invalid_image_format',    
    ])  
  
  }
```

