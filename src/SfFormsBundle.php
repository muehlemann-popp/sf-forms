<?php

namespace SfFormsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * 
 * @author Alex Oleshkevich <alex.oleshkevich@muehlemann-popp.ch>
 */
class SfFormsBundle extends Bundle
{
    
}
