<?php

namespace SfFormsBundle\Translation;

interface TranslationInterface
{

    public function setField($field);

    public function getField();

    public function setObject($object);

    public function getObject();

    public function setLocale($locale);

    public function getLocale();

    public function setContent($content);

    public function getContent();
}
