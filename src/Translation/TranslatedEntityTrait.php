<?php

namespace SfFormsBundle\Translation;

use Doctrine\Common\Collections\ArrayCollection;
use Traversable;

/**
 * @property ArrayCollection|TranslationInterface[] $translations 
 */
trait TranslatedEntityTrait
{

    /**
     * 
     * @param TranslationInterface $translation
     * @return TranslatedEntityInterface
     */
    public function addTranslation(TranslationInterface $translation)
    {
        $translation->setObject($this);
        $this->translations->add($translation);
        return $this;
    }

    /**
     * 
     * @param string $locale
     * @param string $field
     * @return TranslationInterface
     */
    public function getTranslation($locale, $field)
    {
        foreach ($this->translations as $translation) {
            if ($translation->getField() === $field && $translation->getLocale() === $locale) {
                return $translation;
            }
        }
    }

    /**
     * @param string $field
     * @return array
     */
    public function getTranslatedLocalesByField($field)
    {
        $locales = [];
        foreach ($this->translations as $translation) {
            if ($translation->getField() === $field) {
                $locales[$translation->getLocale()] = (boolean) $translation->getContent();
            }
        }
        return $locales;
    }

    public function getTranslatedFields(array $fields)
    {
        $translations = [];
        foreach ($fields as $field) {
            $locales = $this->getTranslatedLocalesByField(strtolower($field));
            $translations[$field] = $locales;
        }
        return $translations;
    }

    /**
     * 
     * @param ArrayCollection|Traversable|array $translations
     */
    public function setTranslations($translations)
    {
        $this->translations = $translations;
    }

    /**
     * 
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

}
