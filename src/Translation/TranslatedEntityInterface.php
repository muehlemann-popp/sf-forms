<?php

namespace SfFormsBundle\Translation;

interface TranslatedEntityInterface
{

    public function addTranslation(TranslationInterface $translation);

    public function getTranslation($locale, $field);
}
