<?php

namespace SfFormsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * 
 * @author Alex Oleshkevich <alex.oleshkevich@muehlemann-popp.ch>
 */
class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sf_forms');

        $rootNode

            ->end()
        ;

        return $treeBuilder;
    }

}
