<?php

namespace SfFormsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageType extends AbstractType
{

    public function getParent()
    {
        return FileType::class;
    }

    /**
     * Add the image_path option
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined([
            'enable_crop', 'enable_preview', 'preview_height', 'preview_width',
            'image_types', 'msg_invalid_format', 'crop_aspect_ratio', 'crop_save_to',
            'preview_src'
        ]);
        
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }
    
    /**
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['enable_preview'] = $options['enable_preview'] ?? false;
        $view->vars['preview_width'] = $options['preview_width'] ?? null;
        $view->vars['preview_height'] = $options['preview_height'] ?? null;
        
        $view->vars['enable_crop'] = $options['enable_crop'] ?? false;
        $view->vars['crop_aspect_ratio'] = $options['crop_aspect_ratio'] ?? 3 / 4;
        $view->vars['crop_save_to'] = $options['crop_save_to'] ?? null;
        
        $view->vars['image_types'] = $options['image_types'] ?? [];
        $view->vars['msg_invalid_format'] = $options['msg_invalid_format'] ?? 'Invalid image format: %format%. Allowed: %allowed%';
    }
    
}
