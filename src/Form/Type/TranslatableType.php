<?php

namespace SfFormsBundle\Form\Type;

use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use SfFormsBundle\Translation\TranslatedEntityInterface;
use SfFormsBundle\Translation\TranslationInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 *
 * @author Alex Oleshkevich <alex.oleshkevich@gmail.com>
 */
class TranslatableType extends AbstractType
{

    /**
     *
     * @var array
     */
    protected $suppportedTypes = [
        TextType::class,
        TextareaType::class
    ];

    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $type = $options['entry_type'];
        $requiredLocales = $options['required_locales'] !== null ? $options['required_locales'] : $this->requiredLocales;
        if ($options['required'] && $options['required'] === true) {
            array_push($requiredLocales, $options['locale']);
        }
        
        
        if (!in_array($type, $this->suppportedTypes)) {
            throw new Exception(__CLASS__ . ': unsupported entry type "' . $type . '".');
        }
        
        foreach ($options['locales'] as $locale) {
            $fieldOptions = $options['entry_options'];
            $fieldOptions['required'] = in_array($locale, $requiredLocales);
            $builder->add($locale, $type, $fieldOptions);
        }
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            /* @var $form Form */
            $form = $event->getForm();
            $data = $event->getData();
            $fieldName = $form->getName();
            /* @var $entity TranslatedEntityInterface */
            $entity = $form->getParent()->getData();
            
            if ($entity) {
                if (!$entity instanceof TranslatedEntityInterface) {
                    throw new Exception(sprintf('Entity "%s" must implemenent interface "%s"', get_class($entity), TranslatedEntityInterface::class));
                }

                $property = $options['translations_property'];
                if (!property_exists($entity, $property)) {
                    throw new Exception(sprintf('Entity "%s" does not has property named "%s"', get_class($entity), $property));
                }

                $accessor = new PropertyAccessor;
                /* @var $translations TranslationInterface[] */
                $translations = $accessor->getValue($entity, $property);
            } else {
                $translations = new ArrayCollection;
            }

            $data = [
                $options['locale'] => $data
            ];

            foreach ($translations as $translation) {
                if ($translation->getLocale() === $options['locale'] || $translation->getField() !== $fieldName) {
                    continue;
                }

                $data[$translation->getLocale()] = $translation->getContent();
            }

            $event->setData($data);
        });

        $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) use ($options, $requiredLocales) {
            /* @var $form Form */
            $form = $event->getForm();
            $data = $event->getData();
            /* @var $entity TranslatedEntityInterface */
            $entity = $form->getParent()->getData();
            $fieldName = $form->getName();
            $fieldValue = null;
            
            if (!$entity) {
                throw new Exception(__METHOD__ . ': an entity is required. Did you forget to pass entity instance into Controller:createForm(Form, Entity)?');
            }

            foreach ($data as $locale => $value) {
                if (in_array($locale, $requiredLocales) && empty($value)) {
                    $form->get($locale)->addError(new FormError('This field is required', 'Field %field% is required', ['%field%' => $locale]));
                }
//                
                if ($locale === $options['locale']) {
                    $fieldValue = $value;
                    continue;
                }

                $translation = $entity->getTranslation($locale, $fieldName);
                if (!$translation) {
                    /* @var $translation TranslationInterface */
                    $translation = new $options['translations_class'];
                    $translation->setField($fieldName);
                    $translation->setLocale($locale);
                    $translation->setObject($entity);
                    $entity->addTranslation($translation);
                }
                $translation->setContent($value);
            }

            $event->setData($fieldValue);
        });
    }

    /**
     * 
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['locales'] = $options['locales'];
    }

    /**
     * 
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            'translations_property', 'translations_class',
            'locale', 'locales'
        ]);
        $resolver->setDefaults([
            'entry_type' => TextType::class,
            'entry_options' => [],
            'by_reference' => false,
            'required_locales' => null
        ]);
    }

}
